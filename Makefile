
MCU   = atmega644
F_CPU = 8000000UL  
BAUD  = 9600


INCDIR = include
SRCDIR = src
OBJDIR = obj
BINDIR = bin

INCDIRS = -I $(INCDIR) 

PROGRAMMER_TYPE = usbasp
# extra arguments to avrdude: baud rate, chip type, -F flag, etc.
PROGRAMMER_ARGS = -P usb

CC = avr-gcc
OBJCOPY = avr-objcopy
OBJDUMP = avr-objdump
AVRSIZE = avr-size
AVRDUDE = avrdude

TARGET = pwm_stabilizer
SRCFILES = $(wildcard $(SRCDIR)/*.c) 
OBJFILES = $(patsubst $(SRCDIR)/%.c,$(OBJDIR)/%.o,$(SRCFILES)) 

SIMULAVR = ~/temp/simulavr/src/simulavr
SIMULAVR_CFLAGS = -Wl,--section-start=.siminfo=0x900000

## Compilation options, type man avr-gcc if you're curious.
CPPFLAGS = -DF_CPU=$(F_CPU) -DBAUD=$(BAUD)UL -I.
CFLAGS = -Os -g -std=c99 -Wall $(SIMULAVR_CFLAGS)
LDFLAGS = -g
TARGET_ARCH = -mmcu=$(MCU)


$(OBJDIR)/%.o: $(SRCDIR)/%.c Makefile
	$(CC) $(CFLAGS)  $(CPPFLAGS) $(TARGET_ARCH) $(INCDIRS) $(COREINCDIRS) -c $< -o $@ 

$(BINDIR)/$(TARGET).elf: $(OBJFILES)
	$(CC) $(LDFLAGS) $(SIMULAVR_CFLAGS) $(TARGET_ARCH) $^ $(LDLIBS) -o $@

$(BINDIR)/%.hex: $(BINDIR)/%.elf
	 $(OBJCOPY) -j .text -j .data -O ihex $< $@

$(BINDIR)/%.eeprom: $(BINDIR)/%.elf
	$(OBJCOPY) -j .eeprom --change-section-lma .eeprom=0 -O ihex $< $@

all: $(BINDIR)/$(TARGET).hex 


simulavr: $(BINDIR)/$(TARGET).elf
	$(SIMULAVR) -f $(BINDIR)/$(TARGET).elf

debug:
	@echo
	@echo "MCU, F_CPU, BAUD:"  $(MCU), $(F_CPU), $(BAUD)
	@echo	

size:  $(BINDIR)/$(TARGET).elf
	$(AVRSIZE) -C --format=avr --mcu=$(MCU) $(BINDIR)/$(TARGET).elf

flash: $(BINDIR)/$(TARGET).hex 
	$(AVRDUDE) -c $(PROGRAMMER_TYPE) -p $(MCU) $(PROGRAMMER_ARGS) -U flash:w:$<

eeprom: $(BINDIR)/$(TARGET).eeprom 

dump_eeprom:
	$(AVRDUDE) -c $(PROGRAMMER_TYPE) -p $(MCU) $(PROGRAMMER_ARGS) -U eeprom:r:$(BINDIR)/eeprom.raw:r

flash_eeprom: $(BINDIR)/$(TARGET).eeprom
	$(AVRDUDE) -c $(PROGRAMMER_TYPE) -p $(MCU) $(PROGRAMMER_ARGS) -U eeprom:w:$<

show_fuses:
	$(AVRDUDE) -c $(PROGRAMMER_TYPE) -p $(MCU) $(PROGRAMMER_ARGS) -nv	

connect:
	python -m serial.tools.miniterm --cr /dev/ttyUSB0 $(BAUD)

## These targets don't have files named after them
.PHONY: all size flash flash_eeprom eeprom dump_eeprom show_fuses debug connect clean

clean:
	rm -f $(OBJDIR)/*
	rm -f $(BINDIR)/*
