#ifndef _PWM_H_
#define _PWM_H_

#include <command.h>

void pwm_init(void);
void load_params_from_eeprom(void);
Response getVersion(void);
Response setPWM0(uint8_t hval, uint8_t lval);
Response getPWM0(void);
Response setR1(uint8_t hval, uint8_t lval);
Response getR1(void);
Response setR2(uint8_t hval, uint8_t lval);
Response getR2(void);
Response setPWMmin(uint8_t hval, uint8_t lval);
Response getPWMmin(void);
Response setPWMmax(uint8_t hval, uint8_t lval);
Response getPWMmax(void);
Response getPWMout(void);
uint8_t in_range(uint16_t);
uint16_t get_pwm0(void);
    
#endif /* _PWM_H_ */
