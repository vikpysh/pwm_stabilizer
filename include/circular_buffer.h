#include <stdint.h>

#ifndef _CIRCULAR_BUFFER_H_
#define _CIRCULAR_BUFFER_H_

enum cbuf_status {CBUF_OK, CBUF_EMPTY, CBUF_FULL};

#define CBUF_SIZE  256

typedef struct CBuf {
    uint8_t data[CBUF_SIZE];
    uint8_t head;
    uint8_t tail;
    uint8_t size;
} CBuf;


CBuf create_cbuf(uint8_t size);
enum cbuf_status cbuf_write(volatile CBuf* buffer, uint8_t byte);
uint8_t cbuf_write_string(volatile CBuf* buffer, uint8_t* s);
enum cbuf_status cbuf_read(volatile CBuf* buffer, uint8_t *byte);
enum cbuf_status cbuf_peek(volatile CBuf* buffer, uint8_t *byte);
uint8_t cbuf_size(CBuf* buffer);

#endif /* _CIRCULAR_BUFFER_H_ */


