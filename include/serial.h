#ifndef _SERIAL_H_
#define _SERIAL_H_

void serial_init();
void serial_write_byte(uint8_t data);
void serial_write_string(uint8_t* data);
uint8_t serial_read_byte(uint8_t* data);
uint8_t serial_read_string(uint8_t* s);

#endif /* _SERIAL_H_ */
