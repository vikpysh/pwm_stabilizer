#ifndef _COMMAND_H_
#define _COMMAND_H_

typedef struct Command {
    uint8_t code;
    uint8_t hval;
    uint8_t lval;
} Command;


enum response_status {RES_OK, RES_NOOP, RES_FAIL};
typedef struct Response {
    enum response_status status;
    uint8_t dataReady;
    uint8_t data[3];
} Response;
    

void shift_command_buffer(uint8_t data);
uint8_t is_valid_command();
Response do_command(Command* cmd);
uint8_t* serialize_response(Response* res);
    
#endif /* _COMMAND_H_ */
