#include <avr/io.h>   
#include <avr/eeprom.h>
#include <command.h>
#include <icp.h>

#define PWM_MIN 800
#define PWM_MAX 2200

static uint16_t eepromPWM0 EEMEM = (PWM_MAX + PWM_MIN) / 2;
static uint16_t eepromPWMmin EEMEM = PWM_MIN;
static uint16_t eepromPWMmax EEMEM = PWM_MAX;
static uint16_t eepromR1 EEMEM = PWM_MIN + 200;
static uint16_t eepromR2 EEMEM = PWM_MAX - 200;

static uint16_t pwm0;   
static uint16_t pwmMin;
static uint16_t pwmMax;
static uint16_t r1;
static uint16_t r2;

uint16_t bytes2Word(uint8_t hval, uint8_t lval) {
    return ((uint16_t)hval << 8) | lval;
}
void word2Bytes(uint16_t word, uint8_t* bytes) {
    bytes[0] = (uint8_t) (pwm0 >> 8);
    bytes[1] =  (uint8_t) pwm0;
}

void pwm_init(void) {
    DDRD |= (1 << PD4); // enable OC1B 
    // DDRD |= (1 << PD5);
    
  TCCR1A |= (1 << WGM11) | (1 << WGM10);   /* Fast PWM mode, with TOP on OCR1A */
  TCCR1B |= (1 << WGM13) | (1 << WGM12);   /* Fast PWM mode, pt.2 */
  TCCR1B |= (1 << CS11);                     /* PWM Freq = F_CPU/8/256 */
  /* TCCR1A |= (1 << COM1A1);  */
  TCCR1A |= (1 << COM1B1);                /* non-inverted PWM on OCR1B */
  OCR1A = 20000; // Initial period 20000us(50Gz)
}

void load_params_from_eeprom() {
    pwm0 = eeprom_read_word(&eepromPWM0);
    pwmMin = eeprom_read_word(&eepromPWMmin);
    pwmMax = eeprom_read_word(&eepromPWMmax);
    r1 = eeprom_read_word(&eepromR1);
    r2 = eeprom_read_word(&eepromR2);
}
                     

Response getVersion() {
    Response res = {RES_OK, 1, "EF"};
    return res;
}
    
Response setPWM0(uint8_t hval, uint8_t lval) {
    Response res = {RES_FAIL, 0, {}};
    uint16_t data = bytes2Word(hval, lval);
    if (data > r1 && data < r2) {
        pwm0 = data;
        eeprom_update_word(&eepromPWM0, pwm0);
        res.status = RES_OK;
    }
    return res;
}

Response getPWM0() {
    Response res = {RES_OK, 1, {0,0,0}};
    word2Bytes(pwm0, res.data);
    return res;
}

Response setR1(uint8_t hval, uint8_t lval) {
    Response res = {RES_FAIL, 0, {}};
    uint16_t data = bytes2Word(hval, lval);
    if (data > pwmMin && data < r2) {
        r1 = data;
        eeprom_update_word(&eepromR1, r1);
        res.status = RES_OK;
    }
    return res;
}

Response getR1() {
    Response res = {RES_OK, 1, {0,0,0}};
    word2Bytes(r1, res.data);
    return res;
}

Response setR2(uint8_t hval, uint8_t lval) {
    Response res = {RES_FAIL, 0, {}};
    uint16_t data = bytes2Word(hval, lval);
    if (data < pwmMax && data > r1) {
        r2 = data;
        eeprom_update_word(&eepromR2, r2);
        res.status = RES_OK;
    }
    return res;
}
Response getR2() {
    Response res = {RES_OK, 1, {0,0,0}};
    word2Bytes(r2, res.data);
    return res;
}

Response setPWMmin(uint8_t hval, uint8_t lval) {
    Response res = {RES_FAIL, 0, {}};
    uint16_t data = bytes2Word(hval, lval);
    if (data > PWM_MIN && data < r1) {
        pwmMin = data;
        eeprom_update_word(&eepromPWMmin, pwmMin);
        res.status = RES_OK;
    }
    return res;
}
Response getPWMmin() {
    Response res = {RES_OK, 1, {0,0,0}};
    word2Bytes(pwmMin, res.data);
    return res;
}

Response setPWMmax(uint8_t hval, uint8_t lval) {
    Response res = {RES_FAIL, 0, {}};
    uint16_t data = bytes2Word(hval, lval);
    if (data < PWM_MAX && data > r2) {
        pwmMax = data;
        eeprom_update_word(&eepromR1, r1);
        res.status = RES_OK;
    }
    return res;
}
Response getPWMmax() {
    Response res = {RES_OK, 1, {0,0,0}};
    word2Bytes(pwmMax, res.data);
    return res;
}
Response getPWMout() {
    Response res = {RES_OK, 1, {0,0,0}};
    word2Bytes(get_pwm_out(), res.data);
    return res;
}

uint8_t in_range(uint16_t data) {
    return data > r1 && data < r2;
}

uint16_t get_pwm0() {
    return pwm0;
}
