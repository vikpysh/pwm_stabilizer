#include <avr/io.h>
#include <util/setbaud.h>
#include <avr/interrupt.h>
#include "circular_buffer.h"


static volatile CBuf tx_buf;
static volatile CBuf rx_buf;

static inline void enable_receive(void){
    UCSR0B |= (1<<RXCIE0);
}

static inline void enable_transmission(void){
    UCSR0B |= (1<<UDRIE0);
}
 
static inline void disable_transmission(void){
    UCSR0B &= ~(1<<UDRIE0);
}
          
ISR(USART0_UDRE_vect){        
    uint8_t tempy;       
    enum cbuf_status status_tx;         
    status_tx = cbuf_read(&tx_buf, &tempy);      
    if (status_tx == CBUF_EMPTY){      
        disable_transmission();      
    } else {         
        UDR0 = tempy; /* write it out to the hardware serial */     
    }        
} 

ISR(USART0_RX_vect){
    cbuf_write(&rx_buf, UDR0);
}

void initUSART(void) {                                /* requires BAUD */
  UBRR0H = UBRRH_VALUE;                        /* defined in setbaud.h */
  UBRR0L = UBRRL_VALUE;
#if USE_2X
  UCSR0A |= (1 << U2X0);
#else
  UCSR0A &= ~(1 << U2X0);
#endif
                                  /* Enable USART transmitter/receiver */
  UCSR0B = (1 << TXEN0) | (1 << RXEN0);
  UCSR0C = (1 << UCSZ01) | (1 << UCSZ00);   /* 8 data bits, 1 stop bit */
}

void serial_init() {
    initUSART();
    enable_receive();
    /* enable_transmission() */
    tx_buf = create_cbuf(64);
    rx_buf = create_cbuf(64);
}

void serial_write_byte(uint8_t data) {
    if(cbuf_write(&tx_buf, data) == CBUF_OK) {
        enable_transmission();
    }
}

void serial_write_string(uint8_t *data) {
  uint8_t i = 0, r;

  // Yes, this is *supposed* to be assignment rather than comparison, so we
  // break when r is assigned zero.
  while ((r = data[i++]))
    serial_write_byte(r);
}

uint8_t serial_read_byte(uint8_t* data) {
    if (cbuf_read(&rx_buf, data) == CBUF_OK) {
        return 1;
    } else {
        return 0;
    }
}

uint8_t serial_read_string(uint8_t* s) {
    uint8_t count = 0;
    while((cbuf_read(&rx_buf, s++) != CBUF_EMPTY)) {
        count++;
    }
    return count;
}
