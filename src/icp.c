#include <avr/io.h>
#include <avr/interrupt.h>

#define	ICP_CTL		TCCR1B			/* ICP1 interrupt control			*/
#define	ICP_SENSE	ICES1			/* ICP1 interrupt sense (rising/falling) */
#define	ICP_START_SENSE	(1 << ICP_SENSE)	/* start with rising edge	*/

volatile static uint16_t pwm_in, pwm_out, period, icr, start, stop;

ISR(TIMER1_CAPT_vect){        
    icr = ICR1;
    
    uint8_t tccr1b = ICP_CTL;
	ICP_CTL = tccr1b ^ (1 << ICP_SENSE);		/* reverse sense		*/
    /*
	 * If we were waiting for a start edge, then this is the
	 * end/beginning of a period.
	 */
    if ((tccr1b & (1 << ICP_SENSE)) == ICP_START_SENSE) {
        period = icr - start;
        pwm_in = stop - start;
        start = icr;
        OCR1A = period;
    } else {
       stop = icr;
    }
} 

void icp_init(void) {
    // Set the interrupt sense
	ICP_CTL	= ICP_START_SENSE ;
	// Enable Input Capture interrupt.
	TIMSK1	|= (1 << ICIE1);
	return;
}

uint16_t get_pwm_in() {
    return pwm_in;
}
    
void set_pwm_out(uint16_t data) {
    pwm_out = data;
    OCR1B = data;
}

uint16_t get_pwm_out() {
    return pwm_out;
}
