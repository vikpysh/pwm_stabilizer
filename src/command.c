#include <util/crc16.h>
#include <stdint.h>
#include <command.h>
#include <pwm.h>

#define COMMAND_BUFFER_SIZE 5
#define COMMAND_MARKER 0xfa
/* #define COMMAND_MARKER '#' */
/* static uint8_t commands[] = { */
/*     0x01, // get version [1] */
/*     0x02, // set pwm0 [800] */
/*     0x03, // set r1 [800] */
/*     0x04, // set r2 [900] */
/*     0x05, // set pwmmin [800] */
/*     0x06, // set pwmmax [2200] */
/*     0x11, // get pwm (current pwmin) */
/*     0x12, // get pwm0 */
/*     0x13, // get r1 */
/*     0x14, // get r2 */
/*     0x15, // get pwmmin */
/*     0x16, // get pwmmax */
/* } */

static uint8_t command_buffer[COMMAND_BUFFER_SIZE]; //[marker, command, hval, lval, crc]

void shift_command_buffer(uint8_t data) {
    for (uint8_t i = 1; i < COMMAND_BUFFER_SIZE; i++) {
        command_buffer[i - 1] = command_buffer[i];
    }
    command_buffer[COMMAND_BUFFER_SIZE - 1] = data;
}

uint8_t crc8(uint8_t* buffer, uint8_t count) {
    uint8_t crc = 0;
    for (; count > 0; count--)
        crc = _crc8_ccitt_update(crc, *buffer++);
    return crc;
}
    
uint8_t is_valid_command(Command* cmd) {
    if (command_buffer[0] == COMMAND_MARKER && crc8(command_buffer, 4) == command_buffer[4]) {
        cmd->code = command_buffer[1];
        cmd->hval = command_buffer[2];
        cmd->lval = command_buffer[3];
        return 1;
    }
    return 0;
}

Response do_command(Command* cmd) {
    Response res = {RES_FAIL, 0, {}};
    switch (cmd->code) {
    case 0x01:
        return getVersion();
    case 0x11:
        return getPWMout();
    case 0x02:
        return setPWM0(cmd->hval, cmd->lval);
    case 0x12:
        return getPWM0();
    case 0x03:
        return setR1(cmd->hval, cmd->lval);
    case 0x13:
        return getR1();
    case 0x04:
        return setR2(cmd->hval, cmd->lval);
    case 0x14:
        return getR2();
    case 0x05:
        return setPWMmin(cmd->hval, cmd->lval);
    case 0x15:
        return getPWMmin();
    case 0x06:
        return setPWMmax(cmd->hval, cmd->lval);
    case 0x16:
        return getPWMmax();
    default:
        res.status = RES_NOOP;
        return res;
    }
}

uint8_t* serialize_response(Response* res) {
    if (res->status == RES_NOOP) {
        return (uint8_t*) "NOOP";
    }
    if (res->status == RES_FAIL) {
        return (uint8_t*) "FAIL";
    }
    if (res->status == RES_OK) {
        if (res->dataReady) {
            return res->data;
        }
        return (uint8_t*) "OK";
    }
    return (uint8_t*) "";
}
