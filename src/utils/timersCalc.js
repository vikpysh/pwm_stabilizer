
us = 0.000001
Tmax =20000 * us
console.log('Tmax', Tmax/us)
n = 16
N = Math.pow(2, n)
console.log('N', N)
tick = Tmax / N
console.log('tick', tick/us)

pwmMin = 800 * us
OCRmin = pwmMin / tick
console.log('OCRmin', OCRmin)

pwmMax = 2200 * us
OCRmax = pwmMax / tick
console.log('OCRmax', OCRmax)

Fneed = 1/tick
console.log('Fneed', Fneed)
console.log('============')
Fcpu = 8000000 / 2 // 1, 2, 4, 8, 16, 32, 64, 128, 256
p = 1 // 1, 8, 64, 256, 1024

Freal = Fcpu / p
console.log('Freal', Freal)
tick_real = 1/Freal
console.log('tick real', tick_real/us)
Tmax_real = N * tick_real
console.log('Tmax_real', Tmax_real/us)
OCRmin_real = pwmMin / tick_real
console.log('OCRmin_real', OCRmin_real)
OCRmax_real = pwmMax / tick_real
console.log('OCRmax_real', OCRmax_real)
Overflow = Tmax/tick_real
console.log('overflow', Overflow)


/*
Fcpu = 8000000
Fpwm = 50
N=16

//N = Math.pow(2, 10)
p = 128
console.log('prescaler:', p)
tick = 1/(Fcpu/p)
console.log('Over', T/tick, 'ticks')

console.log('tick', tick/0.000001, 'us')

pwmMax = 2200 * 0.000001
console.log('OCRmax', pwmMax/tick )
Fpwm = Fcpu/p/N
console.log('Fpwm', Fpwm)
*/
