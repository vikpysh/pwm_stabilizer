#include "simulavr_info.h"
SIMINFO_DEVICE("atmega644");
SIMINFO_CPUFREQUENCY(F_CPU);
SIMINFO_SERIAL_IN("D0", "-", BAUD);
SIMINFO_SERIAL_OUT("D1", "-", BAUD);

#include <avr/interrupt.h>
#include <stdint.h>
#include "serial.h"
#include "command.h"
#include "pwm.h"
#include "icp.h"

int main(void) {
    load_params_from_eeprom();
    serial_init();
    icp_init();
    pwm_init();
    sei();
    uint8_t prompt[] = "\nAVR> ";
    uint8_t noop[] = "NOOP\n";
    serial_write_string(prompt);
    uint8_t data;
    Command cmd;
    uint16_t pwm_in;

	while (1) {
        if (serial_read_byte(&data)) {
            serial_write_byte(data); // echo
            if (data == '\r' || data == '\n') {
                if (is_valid_command(&cmd)) {
                    Response res = do_command(&cmd);
                    serial_write_string(serialize_response(&res));
                    serial_write_string(prompt);
                } else {
                    serial_write_string(noop);
                    serial_write_string(prompt);
                }
            } else {
                shift_command_buffer(data);
            }
        }
        pwm_in = get_pwm_in();
        if (in_range(pwm_in)) {
            set_pwm_out(get_pwm0());
        } else {
            set_pwm_out(pwm_in);
        }
	}
	return 0;
}
