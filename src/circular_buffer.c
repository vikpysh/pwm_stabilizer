#include "circular_buffer.h"

CBuf create_cbuf(uint8_t size){
    CBuf buffer = {{}, 0, 0, 0};
    buffer.size = size;
    return buffer;
}

uint8_t cbuf_size(CBuf* buffer){
    return buffer->size;
}

enum cbuf_status cbuf_write(volatile CBuf* buffer, uint8_t byte){
    uint8_t next = (((buffer->head)+1) % buffer->size);
 
    if (next == buffer->tail){
        return CBUF_FULL;
    }
 
    buffer->data[buffer->head] = byte;
    buffer->head = next;
        return CBUF_OK;
}

uint8_t cbuf_write_string(volatile CBuf* buffer, uint8_t* s){
    uint8_t count = 0;
    uint8_t r;
    do {
        r = *s++;
        if (cbuf_write(buffer, r) != CBUF_FULL) {
           count++;
        } else {
            break;
        }
    }
    while (r);
    return count;
}
enum cbuf_status cbuf_read(volatile CBuf* buffer, uint8_t *byte){
 
    if (buffer->head == buffer->tail){
        return CBUF_EMPTY;
    }
 
    *byte = buffer->data[buffer->tail];
    buffer->tail = ((buffer->tail+1) % buffer->size);
        return CBUF_OK;
}

enum cbuf_status cbuf_peek(volatile CBuf* buffer, uint8_t *byte){
 
    uint8_t last = ((buffer->size + (buffer->head) - 1) % buffer->size);
    if (buffer->head == buffer->tail){
        return CBUF_EMPTY;
    }
    *byte = buffer->data[last];
    return CBUF_OK;
}

