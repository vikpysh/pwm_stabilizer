/*
 * pwm.c
 *
 * Created: 24.09.2018 10:51:11
 * Author : user
 */ 
#define COMMAND_SIZE 5    //0xfa code hval lval crc

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <avr/eeprom.h>
#include <util/crc16.h>
#include <stdint.h>
//#include "USART.h"
#include "serial.h"

#define ICP_CTL TCCR1B
#define ICP_SENSE ICES1
#define  ICP_START_SENSE (1 << ICP_SENSE)

#define PWM_PERIOD 15328
#define PWM_MIN 800
#define PWM_MAX 2200
#define PWM0 (PWM_MAX + PWM_MIN) / 2
#define R1 (PWM_MIN + PWM0) / 2
#define R2 (PWM0 + PWM_MAX) / 2

#define COMMAND_MARKER 0xfa
#define COMMAND_BUFFER_SIZE 5


static uint16_t eeprom_pwm0 EEMEM = PWM0;
static uint16_t eeprom_pwm_min EEMEM = PWM_MIN;
static uint16_t eeprom_pwm_max EEMEM = PWM_MAX;
static uint16_t eeprom_r1 EEMEM = R1;
static uint16_t eeprom_r2 EEMEM = R2;


volatile static  uint16_t icr, pwm_in, start, stop, period, pwm0 = PWM0, pwm_min = PWM_MIN, pwm_max = PWM_MAX, r1 = R1, r2 = R2, t1=50;
uint8_t command_buffer[COMMAND_BUFFER_SIZE];

ISR(TIMER1_CAPT_vect) {
	icr = ICR1;
	uint8_t tccr1b = ICP_CTL;
	ICP_CTL = tccr1b ^ (1 << ICP_SENSE); // reverse sense
	//TCNT1 = 0x0000;
	if ((tccr1b & (1 << ICP_SENSE)) == ICP_START_SENSE) {
		period = icr - start;
		pwm_in = stop - start;
		start = icr;
	} else {
		stop = icr;
	}
}

ISR(TIMER1_OVF_vect) {
	if (pwm_in < pwm_min ) {
		OCR2B = pwm_min;
	}
	else if (pwm_in >= pwm_min && pwm_in < r1) {
		OCR2B = pwm_in;
	}
	else if (pwm_in >= r1 && pwm_in <= r2) {
		OCR2B = pwm0;
	}
	else if (pwm_in > r2 && pwm_in <= pwm_max) {
		OCR2B =  pwm_in;
	}
	else {
		OCR2B = pwm_max;
	}
	if (t1) {
		t1--;
	} else {
		serial_write_word1(OCR2B);
		t1 = 50;
	}
}

void load_params_from_eeprom() {
		pwm0 = eeprom_read_word(&eeprom_pwm0);
		pwm_min = eeprom_read_word(&eeprom_pwm_min);
		pwm_max = eeprom_read_word(&eeprom_pwm_max);
		r1 = eeprom_read_word(&eeprom_r1);
		r2 = eeprom_read_word(&eeprom_r2);
}

void initTimer1() {
	// normal mode
	TCCR1B |= 1 << CS10; // no prescaling (F_CPU / 1)
	TIMSK1 |= 1 << 0; // timer overflow interrupt enabled
}
void initTimer2() {
	TCCR2A |= (1 << WGM21) | (1 << WGM20); // fast pwm with TOP on OCR2A
	TCCR2B |= (1 << WGM23) | (1 << WGM22); // 
	OCR2A = PWM_PERIOD; //  pwm period in 15328s
	//OCR2B = 1000; // initial pwm_out
	
	TCCR2B |= 1 << CS20; // no prescaling (F_CPU / 1)
	TCCR2A |= 1 << COM2B1; // non-inverted PWM on OC2B
	TOCPMSA0 |= 1 << TOCC2S1; //OC2B on TOCC2
	TOCPMCOE |= 1 << TOCC2OE; // enable TOCC2
}
void initICP1() {
	TCCR1B |= 1 << ICES1; // start with rising edge
	TIMSK1 |= 1 << ICIE1; // enable Input Capture interrupt
}

uint16_t bytes2Word(uint8_t hval, uint8_t lval) {
	return ((uint16_t)hval << 8) | lval;
}

void get_version() {
	serial_write_byte(0x01);
}
void get_pwm0() {
	serial_write_word(pwm0);
}
void set_pwm0() {
	uint16_t data = bytes2Word(command_buffer[2], command_buffer[3]);
	if (data >= r1 && data <= r2) {
		pwm0 = data;
		eeprom_update_word(&eeprom_pwm0, pwm0);
	}
}
void get_pwm_min() {
	serial_write_word(pwm_min);
}
void set_pwm_min() {
	uint16_t data = bytes2Word(command_buffer[2], command_buffer[3]);
	if (data >= PWM_MIN && data <= r1) {
		pwm_min = data;
		eeprom_update_word(&eeprom_pwm_min, pwm_min);
	}
}
void get_pwm_max() {
	serial_write_word(pwm_max);
}
void set_pwm_max() {
	uint16_t data = bytes2Word(command_buffer[2], command_buffer[3]);
	if (data >= r2 && data <= PWM_MAX) {
		pwm_max = data;
		eeprom_update_word(&eeprom_pwm_max, pwm_max);
	}
}
void get_r1() {
	serial_write_word(r1);
}
void set_r1() {
	uint16_t data = bytes2Word(command_buffer[2], command_buffer[3]);
	if (data >= pwm_min && data <= pwm0) {
		r1 = data;
		eeprom_update_word(&eeprom_r1, r1);
	}
}
void get_r2() {
	serial_write_word(r2);
}
void set_r2() {
	uint16_t data = bytes2Word(command_buffer[2], command_buffer[3]);
	if (data >= pwm0 && data <= pwm_max) {
		r2 = data;
		eeprom_update_word(&eeprom_r2, r2);
	}
}
void get_pwm_out() {
	serial_write_word(OCR2B);
}
void get_pwm_in() {
	serial_write_word(pwm_in);
}

void do_command() {
	switch (command_buffer[1]) {
		case 0x01:
		return get_version();
		case 0x02:
		return set_pwm0();
		case 0x12:
		return get_pwm0();
		case 0x15:
		return get_pwm_min();
		case 0x05:
		return set_pwm_min();
		case 0x16:
		return get_pwm_max();
		case 0x06:
		return set_pwm_max();
		case 0x13:
		return get_r1();
		case 0x03:
		return set_r1();
		case 0x14:
		return get_r2();
		case 0x04:
		return set_r2();
		case 0x11:
		return get_pwm_out();
		case 0x17:
		return get_pwm_in();
	}

}

uint8_t crc8(uint8_t* buffer, uint8_t count) {
	uint8_t crc = 0;
	for (; count > 0; count--)
	crc = _crc8_ccitt_update(crc, *buffer++);
	return crc;
}

void shift_command_buffer(uint8_t data) {
	for (uint8_t i = 1; i < COMMAND_BUFFER_SIZE; i++) {
		command_buffer[i - 1] = command_buffer[i];
	}
	command_buffer[COMMAND_BUFFER_SIZE - 1] = data;
}

uint8_t is_valid_command() {
	if (command_buffer[0] == COMMAND_MARKER && crc8(command_buffer, 4) == command_buffer[4]) {
		return 1;
	}
	return 0;
}

int main(void) {
  uint8_t data;
  DDRA |= 1 << PA3; // TOCC2 enable output for pwm out
  load_params_from_eeprom();
  //initUSART();
  serial_init();
  initICP1();
  initTimer1(); // pwm_in measure
  initTimer2(); //pwm_out gen
  sei();
  //printString("HELLO\r\n");                          /* to test */

  // ------ Event loop ------ //
  while (1) {
	   if (serial_read_byte(&data)) {
 		   shift_command_buffer(data);
		   if (is_valid_command()) {
				do_command();
		   }
	   }

   }                                                  /* End event loop */
  return 0;
}