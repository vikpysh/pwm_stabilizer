/*
 * serial.c
 *
 * Created: 27.09.2018 16:39:26
 *  Author: user
 */ 
#include <avr/io.h>
#include <util/setbaud.h>
#include <avr/interrupt.h>
#include "circular_buffer.h"


static volatile CBuf tx_buf;
static volatile CBuf rx_buf;

static volatile CBuf tx_buf1;

static inline void enable_receive0(void){
	UCSR0B |= (1 << RXCIE0);
}

static inline void enable_transmission0(void){
	UCSR0B |= (1 << UDRIE0);
}

static inline void enable_transmission1(void){
	UCSR1B |= (1 << UDRIE1);
}
static inline void disable_transmission0(void){
	UCSR0B &= ~ (1 << UDRIE0);
}

static inline void disable_transmission1(void){
	UCSR1B &= ~ (1 << UDRIE1);
}

ISR(USART1_UDRE_vect){
	uint8_t tempy;
	enum cbuf_status status_tx;
	status_tx = cbuf_read(&tx_buf1, &tempy);
	if (status_tx == CBUF_EMPTY){
		disable_transmission1();
		} else {
		UDR1 = tempy; /* write it out to the hardware serial */
	}
}

ISR(USART0_UDRE_vect){
	uint8_t tempy;
	enum cbuf_status status_tx;
	status_tx = cbuf_read(&tx_buf, &tempy);
	if (status_tx == CBUF_EMPTY){
		disable_transmission0();
		} else {
		UDR0 = tempy; /* write it out to the hardware serial */
	}
}

ISR(USART0_RX_vect){
	cbuf_write(&rx_buf, UDR0);
}

void initUSART0(void) {                                /* requires BAUD */
	UBRR0H = UBRRH_VALUE;                        /* defined in setbaud.h */
	UBRR0L = UBRRL_VALUE;
	#if USE_2X
	UCSR0A |= (1 << U2X0);
	#else
	UCSR0A &= ~(1 << U2X0);
	#endif
	/* Enable USART transmitter/receiver */
	UCSR0B = (1 << TXEN0) | (1 << RXEN0);
	UCSR0C = (1 << UCSZ01) | (1 << UCSZ00);   /* 8 data bits, 1 stop bit */
}

void initUSART1(void) {                                /* requires BAUD */
	UBRR1H = UBRRH_VALUE;                        /* defined in setbaud.h */
	UBRR1L = UBRRL_VALUE;
	#if USE_2X
	UCSR1A |= (1 << U2X1);
	#else
	UCSR1A &= ~(1 << U2X1);
	#endif
	/* Enable USART transmitter/receiver */
	UCSR1B = (1 << TXEN1) | (1 << RXEN1);
	UCSR1C = (1 << UCSZ11) | (1 << UCSZ10);   /* 8 data bits, 1 stop bit */
}


void serial_init() {
	initUSART0();
	initUSART1();
	enable_receive0();
	/* enable_transmission() */
	tx_buf = create_cbuf(16);
	rx_buf = create_cbuf(16);
	
	tx_buf1 = create_cbuf(16);
}

void serial_write_byte(uint8_t data) {
	if(cbuf_write(&tx_buf, data) == CBUF_OK) {
		enable_transmission0();
	}
}
void serial_write_byte1(uint8_t data) {
	if(cbuf_write(&tx_buf1, data) == CBUF_OK) {
		enable_transmission1();
	}
}
void serial_write_word(uint16_t data) {
	serial_write_byte((uint8_t) (data >> 8));
	serial_write_byte((uint8_t) data);
}
void serial_write_word1(uint16_t data) {
	serial_write_byte1((uint8_t) (data >> 8));
	serial_write_byte1((uint8_t) data);
}
void serial_write_string(uint8_t *data) {
	uint8_t i = 0, r;

	// Yes, this is *supposed* to be assignment rather than comparison, so we
	// break when r is assigned zero.
	while ((r = data[i++]))
	serial_write_byte(r);
}

uint8_t serial_read_byte(uint8_t* data) {
	if (cbuf_read(&rx_buf, data) == CBUF_OK) {
		return 1;
		} else {
		return 0;
	}
}

uint8_t serial_read_string(uint8_t* s) {
	uint8_t count = 0;
	while((cbuf_read(&rx_buf, s++) != CBUF_EMPTY)) {
		count++;
	}
	return count;
}
