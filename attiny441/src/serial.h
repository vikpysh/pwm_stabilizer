/*
 * serial.h
 *
 * Created: 27.09.2018 16:38:02
 *  Author: user
 */ 


#ifndef SERIAL_H_
#define SERIAL_H_

void serial_init();
void serial_write_byte(uint8_t data);
void serial_write_byte1(uint8_t data);

void serial_write_word(uint16_t data);
void serial_write_word1(uint16_t data);
void serial_write_string(uint8_t* data);
uint8_t serial_read_byte(uint8_t* data);
uint8_t serial_read_string(uint8_t* s);



#endif /* SERIAL_H_ */